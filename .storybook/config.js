import { configure } from '@storybook/react';
import 'bootstrap/dist/css/bootstrap.css'

const req = require.context('../components', true, /.stories.js$/);

function loadStories() {
  req.keys().forEach(filename => req(filename));
}

configure(loadStories, module);