import React, {useState, useEffect} from 'react'
import propTypes from 'prop-types';

import useApiRequest from '../../redux'

import { MDBContainer,MDBRow, MDBCol, MDBCard, MDBCardBody, MDBBtn } from "mdbreact";
import Evaluator from '../Evaluator/Evaluator'
import TraitsStep from '../TraitsStep/TraitsStep'

Assessment.propTypes = {
    // traits: propTypes.array.isRequired,
  };

const trait = {
    question: 'Wellingness to take risks',
    top: 'Risk Taker + Competitive',
    bottom: 'Avoid Risk'
}

function TraitsSteps (amount){
    return (
        <MDBContainer>
            <MDBRow between>
                <TraitsStep key={1} traitNumber={1} isCurrent />
                {disabledButtons(amount)}
            </MDBRow>
        </MDBContainer>
    )
}

function disabledButtons(amount){
    const arrayDisabledButtons = []
    for (let index = 1; index < amount; index++) {
        arrayDisabledButtons.push(<TraitsStep key={index+1} traitNumber={index+1} />);       
    }
    return arrayDisabledButtons
}

export default function Assessment (){
    // const [traits,setTraits] = useState([])
    // const [{status, response}, makeRequest] = useApiRequest('',{verb:'get'})
    // useEffect(()=>{
    //     makeRequest()
    // })

    return (
        <MDBCard>
            <MDBCardBody>
                <MDBContainer>
                    <MDBRow>
                        <MDBCol>
                            <div id='person'>
                                <b>10TRAITS ASSESSMENT</b>
                            </div>
                        </MDBCol>
                    </MDBRow>
                    <MDBRow>
                        <MDBCol>
                            {TraitsSteps(10)}
                        </MDBCol>
                    </MDBRow>
                    <MDBRow>
                        <MDBCol>
                            <div id='person'>
                                {trait.question}
                            </div>
                        </MDBCol>
                    </MDBRow>
                    <MDBRow>
                        <MDBCol>
                            <Evaluator userId={1} traitId={1}/>
                        </MDBCol>
                        <MDBCol>
                            <div id='person'>
                                {trait.top}
                                <i className='fa fa-info-circle'></i>
                            </div>
                        </MDBCol>
                        <MDBCol>
                            <Evaluator userId={2} traitId={1} screenPosition='right' />
                        </MDBCol>
                    </MDBRow>
                    <MDBRow>
                        <MDBCol></MDBCol>
                        <MDBCol>
                            <div id='person'>
                                {trait.bottom}
                                <i className='fa fa-info-circle'></i>
                            </div>
                        </MDBCol>
                        <MDBCol></MDBCol>
                    </MDBRow>
                </MDBContainer>
                <MDBRow end>
                    <MDBCol size='3'>
                        <MDBBtn id='btnNext'>Next</MDBBtn>
                    </MDBCol>
            </MDBRow>
            </MDBCardBody>         
        </MDBCard>
    )
}