import React from 'react'
import { storiesOf } from '@storybook/react'
import './Assessment.css'
import Assessment from './Assessment'

// material design bootstrap imports
import '@fortawesome/fontawesome-free/css/all.min.css'
import 'bootstrap-css-only/css/bootstrap.min.css'
import 'mdbreact/dist/css/mdb.css'

storiesOf('Assessment', module).add('principal', () => {
  return <Assessment />
})
