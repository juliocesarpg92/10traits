import React from 'react'
import propTypes from 'prop-types';
import 'rc-slider/assets/index.css';
import Slider from 'rc-slider/lib/Slider';
import Map from 'react-lodash/lib/Map'
import Reverse from 'react-lodash/lib/Reverse'
import { MDBListGroup, MDBListGroupItem, MDBContainer,MDBRow, MDBCol } from "mdbreact";

Evaluator.propTypes = {
  userId: propTypes.number.isRequired,
  traitId: propTypes.number.isRequired,
  screenPosition: propTypes.string,
};

Evaluator.defaultProps = {
  screenPosition: 'left',
};

const indicators = ['Extreme','Very Strong','Strong','Moderate']

function Evaluator ({userId,traitId,screenPosition}) {

  const isRightAligned = screenPosition === 'right'
  const view = 
  <MDBContainer end={isRightAligned ? true : undefined}>
    <MDBRow end={isRightAligned ? true : undefined}>
      <MDBCol className="px-lg-0" sm="5" md="5" lg="10"> 
        <div id="person">
          <b>Nombre</b>
        </div>
      </MDBCol>
    </MDBRow>
    <MDBRow end={isRightAligned ? true : undefined}>
      {isRightAligned && renderSlider()}
      <MDBCol className="px-lg-0" sm="5" md="5" lg="10">
        <MDBListGroup flush='true'>
          <Map collection={indicators} iteratee={i => <MDBListGroupItem key={i} id="up"><b>{i}</b></MDBListGroupItem>} />
          <MDBListGroupItem id="middle"></MDBListGroupItem>
          <Reverse array={indicators}>
            {indicators => <Map collection={indicators} iteratee={i => <MDBListGroupItem key={i} id="down"><b>{i}</b></MDBListGroupItem>} />}
          </Reverse>
        </MDBListGroup>
      </MDBCol>
      {!isRightAligned && renderSlider()}
    </MDBRow>
  </MDBContainer>

    return view
}

function renderSlider() {
  return (
    <MDBCol id="slider" sm="auto" md="auto" lg="auto">
      <Slider vertical min={0} max={100} defaultValue={50} />      
    </MDBCol>
  )
}

export default Evaluator;