import React from 'react'
import { storiesOf } from '@storybook/react'
import './Evaluator.css'
import Evaluator from './Evaluator'

// material design bootstrap imports
import '@fortawesome/fontawesome-free/css/all.min.css'
import 'bootstrap-css-only/css/bootstrap.min.css'
import 'mdbreact/dist/css/mdb.css'

storiesOf('Evaluator', module)
  .add('just indicators left positioned', () => {
    return <Evaluator userId={1} traitId={1} />
  })
  .add('just indicators right positioned', () => {
    return <Evaluator userId={2} traitId={1} screenPosition='right' />
  })
