import React from 'react'

import { MDBContainer, MDBFooter} from "mdbreact";

export default function Footer (){
    return (
        <div>
            <MDBContainer fluid className='text-center font-small'>
                <h1>10TRAITS</h1>
                <p>Your access and use of this website are subject to our <a href='#'>Privacy Policy</a></p>
                <br/>
                <p>
                    In 10Traits you have 10 powerful traits that are exactly what's needed,<br/>
                    urgently needed, in the world today. When you learn about these traits, study them and reflect on them,<br/>
                    they will manifest as inner power, courage and will to express your full potential.
                </p>
                <br/>
                <p>&copy; {new Date().getFullYear()} 10Traits LLC, 1035 Pearl Street, Boulder, CO80302}</p>
            </MDBContainer>
        </div>
    )
}