import React from 'react'
import { storiesOf } from '@storybook/react'
import './Footer.css'
import Footer from './Footer'

// material design bootstrap imports
import '@fortawesome/fontawesome-free/css/all.min.css'
import 'bootstrap-css-only/css/bootstrap.min.css'
import 'mdbreact/dist/css/mdb.css'

storiesOf('Footer', module).add('principal', () => {
  return <Footer />
})
