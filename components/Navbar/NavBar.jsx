import React from 'react'

import { MDBNavbar, MDBNavbarBrand, MDBNavItem,MDBRow, MDBCol} from "mdbreact";

export default function NavBar (){
    return (
        <MDBNavbar>
            <MDBNavbarBrand>
                <strong>10TRAITS</strong>
            </MDBNavbarBrand>
            <MDBRow center>
                <span>
                    <a href='#'>
                        <strong>ABOUT US</strong>
                    </a>
                </span>
                <span>
                    <a href='#'>
                        <strong><u>OUR ADVISERS</u></strong>
                    </a>
                </span>
                <span>
                    <a href='#'>
                        <strong>HOW 10TRAITS WORKS</strong>
                    </a>
                </span>
                <span>
                    <a href='#'>
                        <strong id='phoneNumber'>303-443-3697</strong>
                    </a>
                </span>
                <span id='splitButton'>
                    <a href='#'>
                        <strong id='signIn'>Sign In</strong>
                    </a>
                    |
                    <a href='#'>
                        <u><strong id='register'>Register</strong></u>
                    </a>
                </span>
            </MDBRow>
        </MDBNavbar>
    )
}