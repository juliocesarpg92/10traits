import React from 'react'
import { storiesOf } from '@storybook/react'
import './NavBar.css'
import NavBar from './NavBar'

// material design bootstrap imports
import '@fortawesome/fontawesome-free/css/all.min.css'
import 'bootstrap-css-only/css/bootstrap.min.css'
import 'mdbreact/dist/css/mdb.css'

storiesOf('NavBar', module).add('principal', () => {
  return <NavBar />
})
