import React from 'react'
import propTypes, { func } from 'prop-types';

import { MDBContainer,MDBRow, MDBCol, MDBCard, MDBCardBody, MDBBtn, MDBInput } from "mdbreact";

let assessmentType = 'Relationship'

function setAssessmentType(event) {
    assessmentType = event.target.value
}

export default function StartDialog() {
    return (
        <MDBCard>
            <MDBContainer fluid>
                <h4>10Traits Tool</h4>
                <p>You have 10 powerful tratis to assess a relationship. Each trait is paired with its opposite.<br/>
                You can evaluate just one person or two.</p>
                <MDBRow className='select-wrapper no-gutters'>
                    <MDBCol lg="4">
                        <label htmlFor="select">Assessment Type</label>
                        <select id='select' className='browser-default custom-select' onChange={setAssessmentType}>
                            <option value='Relationship'>Relationship</option>
                            <option value='Self'>Self</option>
                        </select>
                    </MDBCol>
                </MDBRow>
                <MDBRow className='no-gutters'>
                    <MDBCol lg="6">
                        <MDBInput label='Name of Assessed Person' />
                        {assessmentType === 'Relationship'
                            ? 
                            <MDBInput label='Name of Other Person' />
                            :
                            null}
                    </MDBCol>
                </MDBRow>
                <MDBRow end className='no-gutters'>
                    <MDBCol size='2'>
                        <MDBBtn id='beginBtn'>Begin</MDBBtn>
                    </MDBCol>
                </MDBRow>
            </MDBContainer>
        </MDBCard>
    )
}