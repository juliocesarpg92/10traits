import React from 'react'
import { storiesOf } from '@storybook/react'
import './StartDialog.css'
import StartDialog from './StartDialog'

// material design bootstrap imports
import '@fortawesome/fontawesome-free/css/all.min.css'
import 'bootstrap-css-only/css/bootstrap.min.css'
import 'mdbreact/dist/css/mdb.css'

storiesOf('Start Dialog', module).add('principal', () => {
  return <StartDialog />
})
