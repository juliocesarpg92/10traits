import React from 'react'
import propTypes from 'prop-types';

import { MDBBtn } from "mdbreact";

TraitsStep.propTypes = {
    traitNumber: propTypes.number.isRequired,
    isCurrent: propTypes.bool,
    isDone: propTypes.bool
}

TraitsStep.defaultProps = {
    isCurrent: false,
    isDone: false
}

export default function TraitsStep ({traitNumber, isCurrent, isDone}){
    return (
        <MDBBtn 
            key={traitNumber} 
            id={traitNumber} 
            className={'round '+
            (!isCurrent && !isDone ? 'disabled ' : '')+
            (traitNumber >= 10 ? 'two-numbers' : '')} 
            >
                <b>{isDone ? <i id='done' class='fa fa-check'></i> : traitNumber}</b>
        </MDBBtn>
    )
}