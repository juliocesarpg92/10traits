import React from 'react'
import { storiesOf } from '@storybook/react'
import './TraitsStep.css'
import TraitsStep from './TraitsStep'

// material design bootstrap imports
import '@fortawesome/fontawesome-free/css/all.min.css'
import 'bootstrap-css-only/css/bootstrap.min.css'
import 'mdbreact/dist/css/mdb.css'

storiesOf('TraitsStep', module)
  .add('Current Step', () => {
    return <TraitsStep traitNumber={1} isCurrent/>
  })
  .add('Step Done', () => {
    return <TraitsStep traitNumber={1} isDone/>
  })
  .add('Step Disabled', () => {
    return <TraitsStep traitNumber={1}/>
  })
  .add('Step with two numbers', () => {
    return <TraitsStep traitNumber={10} isCurrent/>
  })
