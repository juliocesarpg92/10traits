import React from 'react'
import { storiesOf } from '@storybook/react'

import './Assessment/Assessment.css'
import './Footer/Footer.css'
import './StartDialog/StartDialog.css'
import './Navbar/NavBar.css'

import Assessment from './Assessment/Assessment'
import NavBar from './Navbar/NavBar'
import Footer from './Footer/Footer'
import StartDialog from './StartDialog/StartDialog'

// material design bootstrap imports
import '@fortawesome/fontawesome-free/css/all.min.css'
import 'bootstrap-css-only/css/bootstrap.min.css'
import 'mdbreact/dist/css/mdb.css'

storiesOf('Preview', module)
  .add('Start Dialog', () => {
    return (
      <div>
        <NavBar />
        <StartDialog />
        <Footer />
      </div>
    )
  })
  .add('Questions', () => {
    return (
      <div>
        <NavBar />
        <Assessment />
        <Footer />
      </div>
    )
  })
